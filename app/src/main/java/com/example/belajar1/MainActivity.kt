package com.example.belajar1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

/**
 * DiceRoller demonstrates simple interactivity in an Android app.
 * It contains one button that updates an image view with a dice
 * vector image with a random value between 1 and 6.
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get the Button view from the layout and assign a click
        // listener to it.
        val rollButton: Button = findViewById(R.id.btn_roll)
        rollButton.setOnClickListener{rollDice()}
    }
    /**
     * Click listener for the rollButton button.
     */
    private fun rollDice(){
        val randomInt = (1..6).random()

        val diceImage: ImageView = findViewById(R.id.dice_image)
        val drawableResources = when (randomInt){
            1 -> R.drawable.dice_1
            2 ->  R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        diceImage.setImageResource(drawableResources)
    }
}
